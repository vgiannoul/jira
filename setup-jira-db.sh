#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER jira WITH SUPERUSER PASSWORD '123';;
    createdb -O jira jira
EOSQL
